
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { CharactersComponent } from './characters/characters.component';
import { HousesComponent } from './houses/houses.component';
import { HouseService } from './house.service';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,

    CharactersComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'characters', component: CharactersComponent },
      { path: 'houses', component: HousesComponent }
    ]),
  ],
  providers:[ HouseService], 
  bootstrap: [AppComponent],
})
export class AppModule {}
