import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface House {
    house: string;
}

@Injectable()
export class HouseService{

    constructor(private http: HttpClient) {}

    fetchHouse() : Observable<House> {
        return this.http.get<House>('https://www.potterapi.com/v1/sortingHat');
    }
}
