import { Component} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { HouseService } from './house.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'potter';

  home$;
  //home2 = JSON.parse(this.home$);
  constructor(private houseService: HouseService) {}

  fetchHouse() {
    this.home$ = this.houseService.fetchHouse();
  }

}
